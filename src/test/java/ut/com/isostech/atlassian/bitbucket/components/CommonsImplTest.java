/*
   Copyright 2017 Isos Technology, LLC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
package ut.com.isostech.atlassian.bitbucket.components;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @since 3.5
 */
public class CommonsImplTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //CommonsImpl testClass = new CommonsImpl();

        throw new Exception("CommonsImpl has no tests!");

    }

}
