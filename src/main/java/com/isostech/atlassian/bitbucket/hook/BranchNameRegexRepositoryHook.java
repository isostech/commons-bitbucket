/*
   Copyright 2017 Isos Technology, LLC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.isostech.atlassian.bitbucket.hook;

import java.util.regex.Pattern;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.bitbucket.hook.repository.PreRepositoryHook;
import com.atlassian.bitbucket.hook.repository.PreRepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.RepositoryHookRequest;
import com.atlassian.bitbucket.hook.repository.RepositoryHookResult;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefChangeType;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class BranchNameRegexRepositoryHook implements PreRepositoryHook<RepositoryHookRequest>{

    final private I18nService i18Service;

    @Inject
    public BranchNameRegexRepositoryHook(@ComponentImport I18nService i18nService) {
        this.i18Service = i18nService;
    }

    public RepositoryHookResult preUpdate(PreRepositoryHookContext context, RepositoryHookRequest request) {
        if (StringUtils.equals("branch-create", request.getTrigger().getId()) || StringUtils.equals("push", request.getTrigger().getId()) ) {
            for (RefChange refChange: request.getRefChanges()) {
                if (refChange.getType() == RefChangeType.ADD) {
                    String branchName = StringUtils.removeStart( refChange.getRef().getId() , "refs/heads/");
                    String branchNameRegex = context.getSettings().getString("branchNameRegex");
                    if( !Pattern.compile(branchNameRegex).matcher(branchName).matches()) {
                        return RepositoryHookResult.rejected( i18Service.getMessage("branch-name-pattern.failure"),
                                i18Service.getMessage("branch-name-pattern.failure-details", branchNameRegex));
                    }
                }
            }
        }
        return RepositoryHookResult.accepted();
    }
}
