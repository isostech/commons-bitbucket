/*
   Copyright 2017 Isos Technology, LLC.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package com.isostech.atlassian.bitbucket.components;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.scope.Scope;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.bitbucket.setting.SettingsValidator;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Named
public class CommonsBitbucketSettings implements SettingsValidator {
    public static final String BRANCH_NAME_PATTERN_FIELD = "branchNameRegex";
    private final I18nService i18nService;

    @Inject
    public CommonsBitbucketSettings(@ComponentImport I18nService i18nService) {
        this.i18nService = i18nService;
    }


    public void validate(Settings settings, SettingsValidationErrors errors, Scope scope) {
        String namePattern = settings.getString(BRANCH_NAME_PATTERN_FIELD);
        try {
            Pattern.compile(namePattern);
        } catch (PatternSyntaxException pe) {
            errors.addFieldError("branchNameRegex", i18nService.getMessage("branch-name-pattern-setting.failure"));
        }
    }

}
